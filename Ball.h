#pragma once
#include "Vec3d.h"
#include "Ray.h"
#include "Object.h"

template <typename T>
struct Ball : public Object{
	Vec3d<T> position;
	T radius;
	Color color;
	Color getColor() override {return color;}
	bool intersects(const Ray) const override;
};


