#pragma once
template <typename T>
struct Vec3d{
	T x;
	T y;
	T z;
};

template <typename T>
Vec3d<T> operator+(const Vec3d<T> a, const Vec3d<T> b){
	return {a.x + b.x, a.y + b.y, a.z + b.z};
}

template <typename T>
Vec3d<T> operator-(const Vec3d<T> a, const Vec3d<T> b){
	return {a.x - b.x, a.y - b.y, a.z - b.z};
}

template<typename T, typename Scalar>
Vec3d<T> operator*(const Scalar s, const Vec3d<T> v){
	return {s * v.x, s * v.y, s * v.z};
}

template <typename T>
T dot(const Vec3d<T> a, const Vec3d<T> b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}
