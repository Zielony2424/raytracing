#pragma once
#include "Color.h"
#include "Ray.h"

struct Object{
	virtual bool intersects(const Ray) const;
	virtual Color getColor();
};
