#pragma once
#include "Color.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdlib>

class Framebuffer{
	std::vector<Color> buffer;
	std::size_t height;
	std::size_t width;
	public:
	Framebuffer(const std::size_t, const std::size_t);
	Color& at(const std::size_t, const std::size_t);
};
